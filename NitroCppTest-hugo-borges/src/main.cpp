#include <iostream>
#include <string>
#include <vector>

#include "model/Utils.hpp"
#include "model/FileParser.hpp"
#include "model/Intersection.hpp"
#include "model/Rectangle.hpp"

using namespace std;

int main(int argc, char *argv[])
{
	string json_file_path;

	// Was called with wrong arguments
	if (argc != 2)
	{
		cout << "Error: wrong usage!" << endl;
		cout << "usage: " << string(argv[0]) << " \"json file path\"" << endl;
		return -1;
	}

	json_file_path = string(argv[1]);
	FileParser parser(json_file_path);

	if (!parser.fileExists())
	{
		cout << "Error: file " << json_file_path << " not found!" << endl;
		return -2;
	}

	vector<Rectangle> rectangles;
	switch(parser.parse(rectangles))
	{
		case FileParser::Error::OK:
			break;

		case FileParser::Error::BAD_FORMAT:
			// The file is not well formatted. The content is not a valid JSON
			cout << "Error: " << json_file_path << " content bad formatted" << endl;
			cout << "Check if it follows JSON standard" << endl;
			return -3;

		case FileParser::Error::RECTS_IS_MISSING:
			cout << "Error: failed to find the \"rects\" object in the JSON content" << endl;
			return -4;

		case FileParser::Error::RECTS_IS_NOT_AN_ARRAY:
			cout << "Error: the \"rects\" object is not an array" << endl;
			cout << "At least two rectangles are required for performing the intersections analysis" << endl;
			return -5;

		case FileParser::Error::RECT_MALFORMATED:
			cout << "Error: the \"rects\" object has invalid dimensions" << endl;
			cout << "Check if there's a negative width or height. They need to be positive" << endl;
			return -6;
	}

	// Print the input as required by the specification
	cout << "Input: " << endl;
	unsigned int i = 1;
	for(vector<Rectangle>::iterator it = rectangles.begin(); it < rectangles.end(); it++, i++) {
		cout << "\t" << i << ": Rectangle at " << it->to_string() << "." << endl;
	}

	cout << endl;
	cout << "Intersections" << endl;
	vector<vector<Intersection>> intersections = Intersection::getIntersections(rectangles);

	if (intersections.size() <= 1)
	{
		cout << "\t None" << endl;
		return 0;
	}

	for(unsigned int level = 1; level < intersections.size(); level++)
	{
		for(unsigned int i = 0; i < intersections[level].size(); i++)
		{
			Intersection *intersection = &intersections[level][i];
			cout << "\t Between rectangle " << Utils::vector_to_string(intersection->regions) << " at " << intersection->rectangle.to_string() << endl;
		}
	}

	return 0;
}
