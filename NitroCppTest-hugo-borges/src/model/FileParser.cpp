#include "model/FileParser.hpp"

#include <fstream>
#include "json.hpp"

#ifdef _WIN32
   #include <io.h>
   #define access    _access
#else
   #include <unistd.h>
#endif

using json = nlohmann::json;

FileParser::FileParser(string filepath)
{
	this->filepath = filepath;
}

bool FileParser::fileExists()
{
	// The file does not exist
	if (access( this->filepath.c_str(), 0 ) != 0)
	{
		return false;
	}

	return true;
}

static FileParser::Error rectangle_from_json(json from, Rectangle &to)
{
	int x;
	int y;
	int height;
	int width;

	try
	{
		x = from.at("x");
		y = from.at("y");
		width = from.at("w");
		height = from.at("h");
	}
	catch(exception &e)
	{
		// Failed when trying to access a parameter of a rectangle object..
		// it may be missing or something
		return FileParser::Error::BAD_FORMAT;
	}

	if ((x < 0) || (y < 0) || (width <= 0) || (height <= 0))
	{
		return FileParser::Error::BAD_FORMAT;
	}

	Rectangle parsed
	(
			(unsigned int) x,
			(unsigned int) y,
			(unsigned int) width,
			(unsigned int) height
	);
	to = Rectangle::clone(parsed);

	return FileParser::Error::OK;
}

FileParser::Error FileParser::parse(vector<Rectangle> &rectangles)
{
	Error result = Error::OK;

	ifstream json_file(this->filepath);
	json json_integral;

	try
	{
		json_integral = json::parse(json_file);
	}
	catch(exception &e)
	{
		return Error::BAD_FORMAT;
	}
	json_file.close();

	// Does the JSON contain the "rects" object?
	json rects;
	try
	{
		rects = json_integral.at("rects");
	}
	catch(exception &e)
	{
		return Error::RECTS_IS_MISSING;
	};

	// The "rects" object is not an array
	if (!rects.is_array())
	{
		return Error::RECTS_IS_NOT_AN_ARRAY;
	}

	// iterate the "rects" array getting the rectangles
	for (json::iterator it = rects.begin(); it != rects.end(); ++it)
	{
		Rectangle rect(0, 0, 0, 0);
		result = rectangle_from_json(*it, rect);
		if (result != Error::OK) return result;

		rectangles.push_back(rect);
	}

	return result;
}
