#include "model/Point.hpp"

#include <sstream>

Point::Point(unsigned int x, unsigned int y)
{
	this->x = x;
	this->y = y;
}

Point::Point()
{
	this->x = 0;
	this->y = 0;
}

bool Point::areEquals(Point &a, Point &b)
{
	if (a.x != b.x) return false;
	if (a.y != b.y) return false;

	return true;
}

std::string Point::to_string()
{
	std::ostringstream result;

	result << "(" << this->x << "," << this->y << ")";

	return result.str();
}
