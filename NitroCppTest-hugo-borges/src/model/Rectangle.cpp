#include "model/Rectangle.hpp"
#include "model/Utils.hpp"

#include <sstream>

Rectangle::Rectangle(unsigned int x, unsigned int y, unsigned int width, unsigned int height) :
	topLeft(x, y),
	topRight(x + width, y),
	bottomLeft(x, y + height),
	bottomRight(x + width, y + height)
{
	this->width = width;
	this->height = height;
}

Rectangle::Rectangle(Point &diag1, Point &diag2) : Rectangle(MIN(diag1.x, diag2.x), MIN(diag1.y, diag2.y), abs((int) diag2.x - (int) diag1.x), abs((int) diag2.y - (int) diag1.y))
{

}

bool Rectangle::contains(Point &point)
{
	// Is the point inside the region limited by the four vertices of the rectangle?
	if (((this->topLeft.x <= point.x) && (point.x <= this->bottomRight.x)) &&
	   ((this->topLeft.y <= point.y) && (point.y <= this->bottomRight.y)))
	{
		return true;
	}

	return false;
}

Rectangle Rectangle::clone(const Rectangle &from)
{
	return Rectangle(from.topLeft.x, from.topLeft.y, from.width, from.height);
}

bool Rectangle::areEquals(Rectangle &rectA, Rectangle &rectB)
{
	if (Point::areEquals(rectA.topLeft, rectB.topLeft) == false) return false;
	if (rectA.width != rectB.width) return false;
	if (rectA.height != rectB.height) return false;

	return true;
}

std::string Rectangle::to_string()
{
	std::ostringstream result;

	/*
	result << "Top Left     = " << this->topLeft.toString()     << endl;
	result << "Top Right    = " << this->topRight.toString()    << endl;
	result << "Bottom Left  = " << this->bottomLeft.toString()  << endl;
	result << "Bottom Right = " << this->bottomRight.toString() << endl;
	*/

	result << this->topLeft.to_string() << ", w=" << this->width << ", h=" << this->height;

	return result.str();
}
