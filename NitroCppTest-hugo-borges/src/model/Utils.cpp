#include "model/Utils.hpp"

string Utils::vector_to_string(vector<unsigned int> input)
{
	string output = "";
	unsigned int i;

	for(i = 0; i < input.size(); i++) input[i] += 1;

	if (input.size() < 2)
	{
		return to_string(input[0]);
	}

	if (input.size() == 2)
	{
		output += to_string(input[0]) + " and " + to_string(input[1]);
		return output;
	}

	for(i = 0; i < input.size() - 2; i++)
	{
		output += to_string(input[i]) + ", ";
	}
	output += to_string(input[i]) + " and " + to_string(input[i + 1]);

	return output;
}
