#include "model/Intersection.hpp"
#include "model/Utils.hpp"

#include <vector>
#include <algorithm>
#include <string>
#include <iostream>

#include <stdlib.h>

using namespace std;

Intersection::Intersection(const Rectangle &rectangle, vector<unsigned int> regions) : rectangle(rectangle)
{
	this->regions = regions;
}

static vector<Point> getPoints(const Rectangle &rectA, const Rectangle &rectB)
{
	Rectangle a = Rectangle::clone(rectA);
	Rectangle b = Rectangle::clone(rectB);

	vector<Point> commonPoints;

	if (a.contains(b.topLeft))     commonPoints.push_back(b.topLeft);
	if (a.contains(b.topRight))    commonPoints.push_back(b.topRight);
	if (a.contains(b.bottomLeft))  commonPoints.push_back(b.bottomLeft);
	if (a.contains(b.bottomRight)) commonPoints.push_back(b.bottomRight);

	return commonPoints;
}


bool Intersection::areIntersected(const Rectangle &rectA, const Rectangle &rectB, Rectangle &intersection)
{
	vector<Point> commonPointsAB = getPoints(rectA, rectB);
	vector<Point> commonPointsBA = getPoints(rectB, rectA);

	if (commonPointsAB.size() == commonPointsBA.size())
	{
		unsigned int commonPointsSize = commonPointsAB.size();
		if (commonPointsSize == 4)
		{
			// They are identical...
			intersection = Rectangle::clone(rectA);
			return true;
		}
		else if (commonPointsSize == 0)
		{
			// They don't intersect
			return false;
		}
		else if (commonPointsSize == 1)
		{
			// The intersection can be obtained by generating a Rectangle through the diagonal points
			intersection = Rectangle(commonPointsAB[0], commonPointsBA[0]);
			return true;
		}
	}

	// There are two vertices of A inside of B
	// or two vertices of B inside A
	vector<Point> *commonPoints;
	const Rectangle *container;
	const Rectangle *insider;

	if (commonPointsAB.size() > commonPointsBA.size())
	{
		commonPoints = &commonPointsAB;
		container = &rectA;
		insider = &rectB;
	}
	else
	{
		commonPoints = &commonPointsBA;
		container = &rectB;
		insider = &rectA;
	}

	unsigned int width = 0;
	unsigned int height = 0;

	if (commonPoints->size() == 2)
	{
		if ((*commonPoints)[0].y == (*commonPoints)[1].y)
		{
			width = insider->width;

			if (container->topLeft.y < insider->topLeft.y)
			{
				height = container->bottomLeft.y - insider->topLeft.y;
				intersection = Rectangle(insider->topLeft.x, insider->topLeft.y, width, height);
				return true;
			}

			height = insider->bottomLeft.y - container->topLeft.y;
			intersection = Rectangle(insider->bottomLeft.x, insider->bottomLeft.y - height, width, height);
			return true;
		}
		else if ((*commonPoints)[0].x == (*commonPoints)[1].x)
		{
			height = insider->height;

			if (container->topLeft.x > insider->topLeft.x)
			{
				width = insider->topRight.x - container->topLeft.x;
				intersection = Rectangle(container->topLeft.x, insider->topLeft.y, width, height);
				return true;
			}
			else
			{
				width = container->topRight.x - insider->topLeft.x;
				intersection = Rectangle(insider->topLeft.x, insider->topLeft.y, width, height);
				return true;
			}
		}
		else
		{
			cout << "Getting intersection: found an unexpected case... reporting it" << endl;
		}
	} else if (commonPoints->size() == 4)
	{
		// The insider rectangle is totally contained inside the container
		intersection = Rectangle::clone(*insider);
		return true;
	}


	cout << "Getting intersection: found an unexpected case... reporting it" << endl;
	return false;
}

vector<vector<Intersection>> Intersection::getIntersections(const vector<Rectangle> &input)
{
	vector<vector<Intersection>> levels;

	unsigned int i = 0;
	unsigned int j = 0;
	unsigned int input_len = input.size();

	// An input with more than two rectangles is mandatory to calculate intersection
	if (input_len <= 1) return levels;

	// Level 0 is composed by the inputs
	unsigned int current_level = 0;
	vector<Intersection> level;
	for(i = 0; i < input_len; i++)
	{
		level.push_back(Intersection(input[i], vector<unsigned int>({i})));
	}
	levels.push_back(level);
	current_level++;

	// Level 1 is composed by the combination of the inputs only
	level.clear();
	for( i = 0; i < input_len; i++)
	{
		for( j = i + 1; j < input_len; j++)
		{
			Rectangle intersectionRectangle(0,0,0,0);
			if (Intersection::areIntersected(input[i], input[j], intersectionRectangle) == false) continue;

			vector<unsigned int> regions;
			regions.push_back(i);
			regions.push_back(j);

			level.push_back(Intersection(intersectionRectangle, regions));
		}
	}
	if (level.size() == 0) return levels;

	levels.push_back(level);
	current_level++;

	// Level 2 and so on is composed by the combination of the inputs and the previous level
	while(true)
	{
		level.clear();

		vector<Intersection> *previous_level = &levels[current_level - 1];
		unsigned int previous_level_len = previous_level->size();
		for( i = 0; i < input_len; i++)
		{
			for(j = 0; j < previous_level_len; j++)
			{
				Intersection *previous_level_intersection_j = &(*previous_level)[j];

				// Skip when comparing an input item with a intersection that is composed by the very same input item
				if (find(previous_level_intersection_j->regions.begin(), previous_level_intersection_j->regions.end(), i) != previous_level_intersection_j->regions.end()) continue;

				Rectangle intersectionRectangle(0,0,0,0);
				if (Intersection::areIntersected(input[i], (*previous_level)[j].rectangle, intersectionRectangle) == false) continue;

				vector<unsigned int> regions;
				regions.insert(regions.end(), previous_level_intersection_j->regions.begin(), previous_level_intersection_j->regions.end());
				regions.push_back(i);
				sort(regions.begin(), regions.end());

				// Check if it is the first occurrence
				bool is_equal = false;
				for(unsigned int k = 0; k < level.size(); k++)
				{
					if (equal(level[k].regions.begin(), level[k].regions.end(), regions.begin()))
					{
						is_equal = true;
						break;
					}
				}
				if (is_equal) continue;

				level.push_back(Intersection(intersectionRectangle, regions));
			}
		}

		// No intersection was add.. no reason to continue
		if (level.size() == 0) return levels;

		levels.push_back(level);
		current_level++;
	}


	return levels;
}
