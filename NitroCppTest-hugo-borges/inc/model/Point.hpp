#ifndef INC_MODEL_POINT_HPP_
#define INC_MODEL_POINT_HPP_

#include <string>

class Point {
public:
	unsigned int x;
	unsigned int y;

	Point();
	Point(unsigned int x, unsigned int y);

	static bool areEquals(Point &a, Point &b);

	std::string to_string();
};


#endif
