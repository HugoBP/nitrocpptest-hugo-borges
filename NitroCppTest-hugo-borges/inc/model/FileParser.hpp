#ifndef INC_MODEL_FILEPARSER_HPP_
#define INC_MODEL_FILEPARSER_HPP_

#include <string>
#include <vector>

#include "model/Rectangle.hpp"

using namespace std;

class FileParser {
private:
	string filepath;

public:
	enum Error{
		OK = 0,
		BAD_FORMAT,
		RECTS_IS_MISSING,
		RECTS_IS_NOT_AN_ARRAY,
		RECT_MALFORMATED,
	};
	FileParser(string filepath);

	bool fileExists();
	Error parse(vector<Rectangle> &rectangles);
};

#endif
