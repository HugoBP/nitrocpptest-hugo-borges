#ifndef INC_MODEL_RECTANGLE_HPP_
#define INC_MODEL_RECTANGLE_HPP_

#include "model/Point.hpp"

class Rectangle {
public:
	Point topLeft;
	Point topRight;
	Point bottomLeft;
	Point bottomRight;

	unsigned int width;
	unsigned int height;

	Rectangle(unsigned int x, unsigned int y, unsigned int width, unsigned int height);
	Rectangle(Point &diag1, Point &diag2);

	static Rectangle clone(const Rectangle &from);
	static bool areEquals(Rectangle &rectA, Rectangle &rectB);
	bool contains(Point &point);

	std::string to_string();
};


#endif
