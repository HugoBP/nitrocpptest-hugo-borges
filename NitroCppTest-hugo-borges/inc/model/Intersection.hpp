#ifndef INC_MODEL_INTERSECTION_HPP_
#define INC_MODEL_INTERSECTION_HPP_

#include "model/Point.hpp"
#include "model/Rectangle.hpp"

#include <vector>

using namespace std;

class Intersection {
public:
	vector<unsigned int> regions;
	Rectangle rectangle;

	Intersection(const Rectangle &rectangle, vector<unsigned int> regions);

	static bool areIntersected(const Rectangle &rectA, const Rectangle &rectB, Rectangle &intersection);
	static vector<vector<Intersection>> getIntersections(const vector<Rectangle> &input);
};

#endif
