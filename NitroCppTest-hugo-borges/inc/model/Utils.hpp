#ifndef INC_MODEL_UTILS_HPP_
#define INC_MODEL_UTILS_HPP_

#include <string>
#include <vector>

#include <stdlib.h>

#define MAX(a, b) ((a >= b) ? a : b)
#define MIN(a, b) ((a >= b) ? b : a)

using namespace std;

class Utils
{
public:
	static string vector_to_string(vector<unsigned int> input);
};

#endif /* INC_MODEL_UTILS_HPP_ */
