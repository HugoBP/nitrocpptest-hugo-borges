Initial steps:

	Make sure you have cmake installed. It's available for Windows, Linux and Mac OS.
	https://cmake.org/

	This project was made and tested in a x86/x64 ubuntu-based linux environment with g++ (Debian 6.3.0-11).
	So if your setup is similar, you should be fine by skipping to the Build instructions directly.
	Otherwise, build the dependencies.

Dependencies:

	-> This project uses the following libraries:
		[X] JSON parser lib by nlohmann
		[X] Google Test Framework for C++

	Both of them are listed under the 3rdparty directory as git submodules.
	If you want to get their source code use the following command line:

	# git submodule update --init --recursive

	This will clone both repositories in 3rdparty/googletest and 3rdparty/json respectively

	The JSON lib is a single header file that must be available at lib/json/include/json.hpp

	The Google Test Framework is easily compiled by using CMake.

	# cd 3rdparty/googletest
	# mkdir build
	# cd build
	# cmake ..
	# make all

	The Google Test Framework output must be available at:

		lib/googletest/include and lib/googletest/link: Header files and libgtest.a respectively
		lib/googlemock/include and lib/googlemock/link: Header files and libgmock.a respectively

Build instructions:

	Once the libs are compiled for you arch/OS and placed on the right folder as explained on the previous session, 
	you can easily build the project by calling the CMake on the project root.

	# cd NitroCppTest-hugo-borges
	# mkdir build
	# cd build
	# cmake ..
	# make all

Running:

	Binary: NitroCppTest-hugo-borges/build/src/NitroCppTest-hugo-borges <json_path>
	Tests:  NitroCppTest-hugo-borges/build/tests/NitroCppTest-hugo-borges_test