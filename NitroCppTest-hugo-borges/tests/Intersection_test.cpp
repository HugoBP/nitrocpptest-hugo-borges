#include <limits.h>
#include <vector>
#include "gtest/gtest.h"
#include "model/Intersection.hpp"

class IntersectionTest : public ::testing::Test
{
 protected:
  virtual void SetUp()
  {
  }

  virtual void TearDown()
  {
  }
};

/*
 * The test cases are illustrated in the doc/intersections.pdf file
 * check it for the picture corresponding to each scenario.
 */

TEST_F(IntersectionTest, test_01)
{
	Rectangle rectA(0,   0, 100, 100);
	Rectangle rectB(120, 0, 100, 100);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, false);

	ASSERT_EQ(intersection.topLeft.x, 300);
	ASSERT_EQ(intersection.topLeft.y, 300);
	ASSERT_EQ(intersection.width, 300);
	ASSERT_EQ(intersection.height, 300);
}

TEST_F(IntersectionTest, test_02)
{
	Rectangle rectA(0,  80, 100, 100);
	Rectangle rectB(80,  0, 100, 100);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 80);
	ASSERT_EQ(intersection.topLeft.y, 80);
	ASSERT_EQ(intersection.width, 20);
	ASSERT_EQ(intersection.height, 20);
}

TEST_F(IntersectionTest, test_03)
{
	// Scenario 3 - A and B overlap in bottom right vertice of A and top left of B
	Rectangle rectA(0,  0, 100, 100);
	Rectangle rectB(80,  80, 100, 100);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 80);
	ASSERT_EQ(intersection.topLeft.y, 80);
	ASSERT_EQ(intersection.width, 20);
	ASSERT_EQ(intersection.height, 20);
}

TEST_F(IntersectionTest, test_04)
{
	Rectangle rectA(80,  0, 100, 100);
	Rectangle rectB(0,  80, 100, 100);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 80);
	ASSERT_EQ(intersection.topLeft.y, 80);
	ASSERT_EQ(intersection.width, 20);
	ASSERT_EQ(intersection.height, 20);
}

TEST_F(IntersectionTest, test_05)
{
	Rectangle rectA(80,  80, 100, 100);
	Rectangle rectB(0,  0, 100, 100);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 80);
	ASSERT_EQ(intersection.topLeft.y, 80);
	ASSERT_EQ(intersection.width, 20);
	ASSERT_EQ(intersection.height, 20);
}

TEST_F(IntersectionTest, test_06)
{
	Rectangle rectA(0,  0, 100, 100);
	Rectangle rectB(80,  10, 80, 80);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 80);
	ASSERT_EQ(intersection.topLeft.y, 10);
	ASSERT_EQ(intersection.width, 20);
	ASSERT_EQ(intersection.height, 80);
}

TEST_F(IntersectionTest, test_07)
{
	Rectangle rectA(60,  0, 100, 100);
	Rectangle rectB(0,  10, 80, 80);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 60);
	ASSERT_EQ(intersection.topLeft.y, 10);
	ASSERT_EQ(intersection.width, 20);
	ASSERT_EQ(intersection.height, 80);
}

TEST_F(IntersectionTest, test_08)
{
	Rectangle rectA(0,  60, 100, 100);
	Rectangle rectB(10,  0, 80, 80);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 10);
	ASSERT_EQ(intersection.topLeft.y, 60);
	ASSERT_EQ(intersection.width, 80);
	ASSERT_EQ(intersection.height, 20);
}

TEST_F(IntersectionTest, test_09)
{
	Rectangle rectA(0,  0, 100, 100);
	Rectangle rectB(10,  80, 80, 80);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 10);
	ASSERT_EQ(intersection.topLeft.y, 80);
	ASSERT_EQ(intersection.width, 80);
	ASSERT_EQ(intersection.height, 20);
}

TEST_F(IntersectionTest, test_10)
{
	Rectangle rectA(0,  0, 100, 100);
	Rectangle rectB(0,  0, 100, 100);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 0);
	ASSERT_EQ(intersection.topLeft.y, 0);
	ASSERT_EQ(intersection.width, 100);
	ASSERT_EQ(intersection.height, 100);
}

TEST_F(IntersectionTest, test_11)
{
	Rectangle rectA(0,  0, 100, 100);
	Rectangle rectB(10,  10, 80, 80);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 10);
	ASSERT_EQ(intersection.topLeft.y, 10);
	ASSERT_EQ(intersection.width, 80);
	ASSERT_EQ(intersection.height, 80);
}

TEST_F(IntersectionTest, test_12)
{
	Rectangle rectA(10,  10, 80, 80);
	Rectangle rectB(0,  0, 100, 100);
	Rectangle intersection(300, 300, 300, 300);

	bool rv = Intersection::areIntersected(rectA, rectB, intersection);
	ASSERT_EQ(rv, true);

	ASSERT_EQ(intersection.topLeft.x, 10);
	ASSERT_EQ(intersection.topLeft.y, 10);
	ASSERT_EQ(intersection.width, 80);
	ASSERT_EQ(intersection.height, 80);
}

TEST_F(IntersectionTest, test_no_intersections)
{
	vector<Rectangle> input;

	input.push_back(Rectangle(0,   0, 10, 10));
	input.push_back(Rectangle(20, 20, 10, 10));
	input.push_back(Rectangle(40, 40, 10, 10));
	input.push_back(Rectangle(60, 60, 10, 10));

	vector<vector<Intersection>> result = Intersection::getIntersections(input);

	// We are expecting no intersections...
	// result[0] = inputs

	ASSERT_EQ(result.size(), 1);
	ASSERT_EQ(true, Rectangle::areEquals(input[0], result[0].at(0).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(input[1], result[0].at(1).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(input[2], result[0].at(2).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(input[3], result[0].at(3).rectangle));
}

// For the following tests check the diagram at doc/intersection-test-cases/n-rectangles-intersections.pdf
TEST_F(IntersectionTest, test_concentric_3)
{
	vector<Rectangle> input;

	input.push_back(Rectangle(80, 80, 10, 10));
	input.push_back(Rectangle(70, 70, 30, 30));
	input.push_back(Rectangle(60, 60, 50, 50));

	vector<Intersection> expectedOutput;
	expectedOutput.push_back(Intersection(Rectangle(80, 80, 10, 10), vector<unsigned int>({1, 2})));
	expectedOutput.push_back(Intersection(Rectangle(80, 80, 10, 10), vector<unsigned int>({1, 3})));
	expectedOutput.push_back(Intersection(Rectangle(70, 70, 30, 30), vector<unsigned int>({2, 3})));
	expectedOutput.push_back(Intersection(Rectangle(80, 80, 10, 10), vector<unsigned int>({1, 2, 3})));

	vector<vector<Intersection>> result = Intersection::getIntersections(input);

	// We are expecting three levels of intersections:
	// result[0] = inputs
	// result[1] = two elements intersections. Example: {1,2}, {1,3} and {2,3}
	// result[2] = three elements intersections. Example: {1,2,3}
	ASSERT_EQ(result.size(), 3);
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[0].rectangle, result[1].at(0).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[1].rectangle, result[1].at(1).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[2].rectangle, result[1].at(2).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[3].rectangle, result[2].at(0).rectangle));
}

TEST_F(IntersectionTest, test_intersection_00)
{
	vector<Rectangle> input;

	input.push_back(Rectangle(30, 50, 60, 60));
	input.push_back(Rectangle(50, 15, 80, 80));
	input.push_back(Rectangle(120, 0, 30, 30));

	vector<Intersection> expectedOutput;
	expectedOutput.push_back(Intersection(Rectangle(50, 50, 40, 45), vector<unsigned int>({1, 2})));
	expectedOutput.push_back(Intersection(Rectangle(120, 15, 10, 15), vector<unsigned int>({2, 3})));

	vector<vector<Intersection>> result = Intersection::getIntersections(input);

	// We are expecting two levels of intersections:
	// result[0] = inputs
	// result[1] = two elements intersections. {1,2} and {2,3}
	ASSERT_EQ(result.size(), 2);
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[0].rectangle, result[1].at(0).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[1].rectangle, result[1].at(1).rectangle));
}

TEST_F(IntersectionTest, test_intersection_01)
{
	vector<Rectangle> input;

	input.push_back(Rectangle( 0,  0,  50,  50));
	input.push_back(Rectangle( 0, 30,  50,  50));
	input.push_back(Rectangle(30,  0, 100, 100));
	input.push_back(Rectangle(20, 20,  50,  50));

	vector<Intersection> expectedOutput;
	expectedOutput.push_back(Intersection(Rectangle( 0, 30, 50, 20), vector<unsigned int>({1, 2})));
	expectedOutput.push_back(Intersection(Rectangle(30,  0, 20, 50), vector<unsigned int>({1, 3})));
	expectedOutput.push_back(Intersection(Rectangle(20, 20, 30, 30), vector<unsigned int>({1, 4})));
	expectedOutput.push_back(Intersection(Rectangle(30, 30, 20, 50), vector<unsigned int>({2, 3})));
	expectedOutput.push_back(Intersection(Rectangle(20, 30, 30, 40), vector<unsigned int>({2, 4})));
	expectedOutput.push_back(Intersection(Rectangle(30, 20, 40, 50), vector<unsigned int>({3, 4})));
	expectedOutput.push_back(Intersection(Rectangle(30, 30, 20, 20), vector<unsigned int>({1, 2, 3})));
	expectedOutput.push_back(Intersection(Rectangle(20, 30, 30, 20), vector<unsigned int>({1, 2, 4})));
	expectedOutput.push_back(Intersection(Rectangle(30, 20, 20, 30), vector<unsigned int>({1, 3, 4})));
	expectedOutput.push_back(Intersection(Rectangle(30, 30, 20, 40), vector<unsigned int>({2, 3, 4})));
	expectedOutput.push_back(Intersection(Rectangle(30, 30, 20, 20), vector<unsigned int>({1, 2, 3, 4})));

	vector<vector<Intersection>> result = Intersection::getIntersections(input);

	// We are expecting four levels of intersections:
	ASSERT_EQ(result.size(), 4);
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[0].rectangle, result[1].at(0).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[1].rectangle, result[1].at(1).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[2].rectangle, result[1].at(2).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[3].rectangle, result[1].at(3).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[4].rectangle, result[1].at(4).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[5].rectangle, result[1].at(5).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[6].rectangle, result[2].at(0).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[7].rectangle, result[2].at(1).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[8].rectangle, result[2].at(2).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[9].rectangle, result[2].at(3).rectangle));
	ASSERT_EQ(true, Rectangle::areEquals(expectedOutput[10].rectangle, result[3].at(0).rectangle));
}
