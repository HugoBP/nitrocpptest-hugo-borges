#include <iostream>

#include <limits.h>

#include "gtest/gtest.h"
#include "model/Rectangle.hpp"

using namespace std;

class RectangleTest : public ::testing::Test
{
 protected:
  virtual void SetUp()
  {
  }

  virtual void TearDown()
  {
  }
};

TEST_F(RectangleTest, constructor)
{
    unsigned int x = 100;
    unsigned int y = 100;
    unsigned int width = 250;
    unsigned int height = 80;

    Rectangle rect(x, y, width, height);

    Point topLeft(100, 100);
    Point topRight(350, 100);
    Point bottomLeft(100, 180);
    Point bottomRight(350, 180);

    EXPECT_TRUE(Point::areEquals(topLeft,    rect.topLeft));
    EXPECT_TRUE(Point::areEquals(topRight,   rect.topRight));
    EXPECT_TRUE(Point::areEquals(bottomLeft, rect.bottomLeft));
    EXPECT_TRUE(Point::areEquals(bottomRight,rect.bottomRight));
}
