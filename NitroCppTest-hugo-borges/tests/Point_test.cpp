#include <limits.h>
#include "gtest/gtest.h"
#include "model/Point.hpp"

class PointTest : public ::testing::Test
{
 protected:
  virtual void SetUp()
  {
  }

  virtual void TearDown()
  {
  }
};

TEST_F(PointTest, empty_constructor)
{
    Point point;

    EXPECT_EQ(0, point.x);
    EXPECT_EQ(0, point.y);
}

TEST_F(PointTest, constructor)
{
    unsigned int x = 4;
    unsigned int y = 5;

    Point point(x, y);

    EXPECT_EQ(x, point.x);
    EXPECT_EQ(y, point.y);
}

TEST_F(PointTest, areEquals){
	Point a(1, 3);
	Point b(2, 4);

	EXPECT_FALSE(Point::areEquals(a, b));

	b.y = 3;
	EXPECT_FALSE(Point::areEquals(a, b));

	a.x = 2;
	EXPECT_TRUE(Point::areEquals(a, b));
}
