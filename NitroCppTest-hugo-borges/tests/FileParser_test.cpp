#include <limits.h>

#include <string>
#include <iostream>

#include "model/FileParser.hpp"

#include "gtest/gtest.h"

using namespace std;

class FileParserTest : public ::testing::Test
{
 protected:
  virtual void SetUp()
  {
  }

  virtual void TearDown()
  {
  }
};

TEST_F(FileParserTest, check_file_existance_bad)
{
	string filepath = "./tests/resources/non-existent-file.json";
	FileParser parser(filepath);

	ASSERT_EQ(false, parser.fileExists());
}

TEST_F(FileParserTest, check_file_existance_good)
{
	string filepath = "./tests/resources/input.json";
	FileParser parser(filepath);

	ASSERT_EQ(false, parser.fileExists());
}

TEST_F(FileParserTest, parse_non_json_file)
{
	string filepath = "./tests/resources/non-json.json";
	FileParser parser(filepath);

	vector<Rectangle> input_rectangles;

	ASSERT_EQ(true, parser.fileExists());
	ASSERT_EQ(FileParser::Error::BAD_FORMAT, parser.parse(input_rectangles));
}

TEST_F(FileParserTest, parse_missing_rects_object_json_file)
{
	string filepath = "./tests/resources/missing-rects-object.json";
	FileParser parser(filepath);

	vector<Rectangle> input_rectangles;

	ASSERT_EQ(true, parser.fileExists());
	ASSERT_EQ(FileParser::Error::RECTS_IS_MISSING, parser.parse(input_rectangles));
}

TEST_F(FileParserTest, parse_rects_not_array_json_file)
{
	string filepath = "./tests/resources/rects-not-array.json";
	FileParser parser(filepath);

	vector<Rectangle> input_rectangles;

	ASSERT_EQ(true, parser.fileExists());
	ASSERT_EQ(FileParser::Error::RECTS_IS_NOT_AN_ARRAY, parser.parse(input_rectangles));
}

TEST_F(FileParserTest, parse_valid_file)
{
	string filepath = "./tests/resources/example-input.json";
	FileParser parser(filepath);

	vector<Rectangle> input_rectangles;

	ASSERT_EQ(true, parser.fileExists());
	ASSERT_EQ(FileParser::Error::OK, parser.parse(input_rectangles));

	ASSERT_EQ(4, input_rectangles.size());

	Rectangle *second = &input_rectangles[1];
	ASSERT_EQ(120, second->topLeft.x);
	ASSERT_EQ(200, second->topLeft.y);
	ASSERT_EQ(250, second->width);
	ASSERT_EQ(150, second->height);
}

